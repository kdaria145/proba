import './App.css';
import {Route, BrowserRouter as Router, Routes} from "react-router-dom";
import Try from "./components/pages/Try/Try";

function App() {
  return (
    <div className="App">
      <Router>
          <Routes>
              <Route path="/try" element={<Try/>}/>
          </Routes>
      </Router>
        <div id="modal"/>
    </div>
  );
}

export default App;
