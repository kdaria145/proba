import './question.scss'

const Question = ({questionID, questionText, answerValues}) => {
    return (
        <>
            <div className="question__container">
                <p className="question__title">{questionText}</p>
                <div className="answers-block">
                    {answerValues.map((value, indexValue) => (
                        <div className="answers-block__answer">
                            <input id={`radio-${questionID}-${indexValue}`} type="radio" name={`radio-${questionID}`} value={indexValue} className="question__input"/>
                            <label key={indexValue} className="question__label" for={`radio-${questionID}-${indexValue}`}>
                                <span>{value}</span>
                            </label>
                        </div>
                    ))}
                </div>
            </div>
        </>
    )
}
export default Question;