import Question from "../../common/Question/Question";
import {tryQuestions} from "../../../utils/tryQuestions";
import './try.scss';
import {Portal} from "../../common/Portal";
import {useState} from "react";
import cifromol from '../../../images/cifromol.png';
import viktoria from '../../../images/viktoriya.jpg';

const Try = () => {
    const [showModal, setShowModal] = useState(false);
    const [showCompanyDesc, setShowCompanyDesc] = useState(false);
    return (
        <>
            <div className="try-page">
                <div className="container-questions">
                    {tryQuestions.map((tryQuestion, questionIndex)=> {
                        return ( <Question key={questionIndex} questionID={questionIndex} questionText={tryQuestion.question} answerValues={tryQuestion.answers}/>)
                    })}
                </div>
                <button className="try-page__show-results" onClick={() => {setShowModal(true)}}>Показать результаты</button>
                { showModal &&
                <Portal>
                    {!showCompanyDesc ?
                        <>
                            <p className="modal__title">Новое совпадение!</p>
                            <div className="modal__block modal__block_person">
                                <img src={viktoria} alt="viktoria" width="50px" height="50px"/>
                                <div className="modal__block_desc">
                                    <b>Виктория</b>
                                    <p>Фрилансер</p>
                                </div>
                            </div>
                            <div className="modal__close">
                                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 25 28" fill="none">
                                    <path d="M10.1074 14.2061L1.0498 0.998047H7.59277L13.7451 10.4463L19.9463 0.998047H26.4404L17.3584 14.2061L26.9043 28H20.4102L13.7451 17.8926L7.10449 28H0.610352L10.1074 14.2061Z" fill="black"/>
                                </svg>
                            </div>
                            <div className="modal__block modal__block_match" onClick={() => {setShowCompanyDesc(true)}}>
                                <img src={cifromol} alt="Cifromol" width="50px" height="50px"/>
                                <div className="modal__block_desc">
                                    <b>Цифромол</b>
                                    <p>Помощник руководителя аналитического отдела</p>
                                </div>
                            </div>
                        </> :
                        <>
                            <p className="modal__title modal__title_company-desc">
                            <div className="company-icon">
                                <img src={cifromol} alt="Cifromol" width="50px" height="50px"/>
                            </div>
                                <span className="company-name">
                                    Цифромол
                                </span>
                            </p>
                            <div className="desc-items">
                                <p className="desc-items__item-name">Должность:</p> <p>Помощник руководителя аналитического отдела</p>
                                <p className="desc-items__item-name">График:</p> <p>Неполный рабочий день</p>
                                <p className="desc-items__item-name">Оплата:</p> <p>25 000 рублей</p>
                            </div>
                        </>
                    }
                </Portal>
                }
            </div>
        </>
    )
}
export default Try;